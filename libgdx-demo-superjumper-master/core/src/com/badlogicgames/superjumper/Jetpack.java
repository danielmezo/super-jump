package com.badlogicgames.superjumper;

/**
 * Created by daniel.mezo on 05/04/2017.
 */

public class Jetpack extends GameObject{

    public static float JETPACK_WIDTH = 1f;
    public static float JETPACK_HEIGHT = 1f;

    public Jetpack (float x, float y) {
        super(x, y, JETPACK_WIDTH, JETPACK_HEIGHT);
    }
}
